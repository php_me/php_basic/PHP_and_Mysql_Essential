<?php // 1. Database connection---------------
$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "cycle_world";
$connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

// 2.Connection varification-----------------
if(mysqli_connect_errno()){
	die("Database Connection failed: ".mysqli_connect_error()."(".mysqli_connect_errno().")");
}

?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	<?php

	$query = "SELECT * ";
	$query .= "FROM subjects ";
	$query .= "WHERE visible = 1 ";
	$query .= "ORDER BY position ASC ";
	$result = mysqli_query($connection, $query); 	
	if(!$result){ die("Database query failed.");}
	
	while($subject = mysqli_fetch_assoc($result)){
		?>
	<li><?php echo $subject["menu_name"]."=".$subject["id"]."<br>"; ?></li>
	
	<?php		
	}
	
	//mysqli_free_result($result);

	
	
	?>
</body>
</html>
<?php mysqli_close($connection);?>