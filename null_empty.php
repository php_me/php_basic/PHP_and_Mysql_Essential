<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title></title>
</head>
<body>
	<?php //null
		$v1 = NULL;
		$v2 = "";
		echo "V1: ".is_null($v1)."<br>";
		echo "V2: ".is_null($v2)."<br>";
		echo "V3: ".is_null($v3)."<br><hr>";
		
		echo "V1: ".isset($v1)."<br>";
		echo "V2: ".isset($v2)."<br>";
		echo "V3: ".isset($v3)."<br><hr>";
		
		// Empty: "", null, 0, 0.0, array(), false
		
		
		$v1 = NULL;
		$v2 = "";
		$v3 = "0";
		echo "V1: ".empty($v1)."<br>";
		echo "V2: ".empty($v2)."<br>";
		echo "V3: ".empty($v3)."<br><hr>";
		
		
	?>
	
</body>
</html>